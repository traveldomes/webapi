namespace TravelDomes.Booking.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accomodation",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                        CompanyId = c.Int(nullable: false),
                        WebSiteUrl = c.String(nullable: false),
                        ContactId = c.Int(nullable: false),
                        AddressId = c.Int(nullable: false),
                        ReservationDeliveryType = c.Int(nullable: false),
                        ReservationEmail = c.String(nullable: false, maxLength: 254),
                        ReservationFax = c.String(nullable: false, maxLength: 15),
                        LocalizationLong = c.Decimal(precision: 11, scale: 8),
                        LocalizationLat = c.Decimal(precision: 11, scale: 8),
                        CurrencyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Company", t => t.CompanyId)
                .ForeignKey("dbo.Contact", t => t.ContactId)
                .ForeignKey("dbo.Currency", t => t.CurrencyId)
                .Index(t => t.CompanyId)
                .Index(t => t.ContactId)
                .Index(t => t.CurrencyId);
            
            CreateTable(
                "dbo.AccomodationAmenity",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AccomodationId = c.Int(nullable: false),
                        AmenityId = c.Int(nullable: false),
                        IsAvailableToAll = c.Boolean(nullable: false),
                        IsFreeOfCharge = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Amenity", t => t.AmenityId)
                .ForeignKey("dbo.Accomodation", t => t.AccomodationId)
                .Index(t => t.AccomodationId)
                .Index(t => t.AmenityId);
            
            CreateTable(
                "dbo.Amenity",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 250),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AccomodationUnitAmenity",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AccomodationUnitId = c.Int(nullable: false),
                        AmenityId = c.Int(nullable: false),
                        IsFreeOfCharge = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AccomodationUnit", t => t.AccomodationUnitId)
                .ForeignKey("dbo.Amenity", t => t.AmenityId)
                .Index(t => t.AccomodationUnitId)
                .Index(t => t.AmenityId);
            
            CreateTable(
                "dbo.AccomodationUnit",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AccomodationId = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 250),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AccomodationType", t => t.Type)
                .ForeignKey("dbo.Accomodation", t => t.AccomodationId)
                .Index(t => t.AccomodationId)
                .Index(t => t.Type);
            
            CreateTable(
                "dbo.AccomodationType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 250),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Rate",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AccomodationUnitId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 250),
                        Description = c.String(),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AccomodationUnit", t => t.AccomodationUnitId)
                .Index(t => t.AccomodationUnitId);
            
            CreateTable(
                "dbo.RatePrice",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RateId = c.Int(nullable: false),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 4),
                        ValidFrom = c.DateTime(nullable: false),
                        ValidTo = c.DateTime(nullable: false),
                        IsValidWeekDay1 = c.Boolean(nullable: false),
                        IsValidWeekDay2 = c.Boolean(nullable: false),
                        IsValidWeekDay3 = c.Boolean(nullable: false),
                        IsValidWeekDay4 = c.Boolean(nullable: false),
                        IsValidWeekDay5 = c.Boolean(nullable: false),
                        IsValidWeekDay6 = c.Boolean(nullable: false),
                        IsValidWeekDay7 = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Rate", t => t.RateId)
                .Index(t => t.RateId);
            
            CreateTable(
                "dbo.RateTax",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RateId = c.Int(nullable: false),
                        TaxId = c.Int(nullable: false),
                        IsIncluded = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tax", t => t.TaxId)
                .ForeignKey("dbo.Rate", t => t.RateId)
                .Index(t => t.RateId)
                .Index(t => t.TaxId);
            
            CreateTable(
                "dbo.Tax",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 4),
                        ValueType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Company",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 250),
                        IdentificationCode = c.String(nullable: false, maxLength: 50),
                        BillingAddressId = c.Int(nullable: false),
                        BillingContactId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Address", t => t.BillingAddressId)
                .ForeignKey("dbo.Contact", t => t.BillingContactId)
                .Index(t => t.BillingAddressId)
                .Index(t => t.BillingContactId);
            
            CreateTable(
                "dbo.Address",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CountryId = c.Int(nullable: false),
                        CityName = c.String(nullable: false, maxLength: 250),
                        StreetLine1 = c.String(maxLength: 250),
                        StreetLine2 = c.String(maxLength: 250),
                        PostalCode = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Contact",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 250),
                        MiddleName = c.String(maxLength: 250),
                        LastName = c.String(maxLength: 250),
                        Salutation = c.String(maxLength: 50),
                        PrimaryPhone = c.String(maxLength: 15, fixedLength: true),
                        MobilePhone = c.String(maxLength: 15, fixedLength: true),
                        Fax = c.String(maxLength: 15, fixedLength: true),
                        Email = c.String(maxLength: 254),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Currency",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 250),
                        IsDefault = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Destination",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                        Latitude = c.Decimal(precision: 11, scale: 8),
                        Longitude = c.Decimal(precision: 11, scale: 8),
                        IsTop = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.HotelDestination",
                c => new
                    {
                        HotelId = c.Int(nullable: false),
                        DestinationId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.HotelId, t.DestinationId })
                .ForeignKey("dbo.Accomodation", t => t.HotelId, cascadeDelete: true)
                .ForeignKey("dbo.Destination", t => t.DestinationId, cascadeDelete: true)
                .Index(t => t.HotelId)
                .Index(t => t.DestinationId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.HotelDestination", "DestinationId", "dbo.Destination");
            DropForeignKey("dbo.HotelDestination", "HotelId", "dbo.Accomodation");
            DropForeignKey("dbo.Accomodation", "CurrencyId", "dbo.Currency");
            DropForeignKey("dbo.Company", "BillingContactId", "dbo.Contact");
            DropForeignKey("dbo.Accomodation", "ContactId", "dbo.Contact");
            DropForeignKey("dbo.Company", "BillingAddressId", "dbo.Address");
            DropForeignKey("dbo.Accomodation", "CompanyId", "dbo.Company");
            DropForeignKey("dbo.AccomodationUnit", "AccomodationId", "dbo.Accomodation");
            DropForeignKey("dbo.AccomodationAmenity", "AccomodationId", "dbo.Accomodation");
            DropForeignKey("dbo.AccomodationUnitAmenity", "AmenityId", "dbo.Amenity");
            DropForeignKey("dbo.Rate", "AccomodationUnitId", "dbo.AccomodationUnit");
            DropForeignKey("dbo.RateTax", "RateId", "dbo.Rate");
            DropForeignKey("dbo.RateTax", "TaxId", "dbo.Tax");
            DropForeignKey("dbo.RatePrice", "RateId", "dbo.Rate");
            DropForeignKey("dbo.AccomodationUnitAmenity", "AccomodationUnitId", "dbo.AccomodationUnit");
            DropForeignKey("dbo.AccomodationUnit", "Type", "dbo.AccomodationType");
            DropForeignKey("dbo.AccomodationAmenity", "AmenityId", "dbo.Amenity");
            DropIndex("dbo.HotelDestination", new[] { "DestinationId" });
            DropIndex("dbo.HotelDestination", new[] { "HotelId" });
            DropIndex("dbo.Company", new[] { "BillingContactId" });
            DropIndex("dbo.Company", new[] { "BillingAddressId" });
            DropIndex("dbo.RateTax", new[] { "TaxId" });
            DropIndex("dbo.RateTax", new[] { "RateId" });
            DropIndex("dbo.RatePrice", new[] { "RateId" });
            DropIndex("dbo.Rate", new[] { "AccomodationUnitId" });
            DropIndex("dbo.AccomodationUnit", new[] { "Type" });
            DropIndex("dbo.AccomodationUnit", new[] { "AccomodationId" });
            DropIndex("dbo.AccomodationUnitAmenity", new[] { "AmenityId" });
            DropIndex("dbo.AccomodationUnitAmenity", new[] { "AccomodationUnitId" });
            DropIndex("dbo.AccomodationAmenity", new[] { "AmenityId" });
            DropIndex("dbo.AccomodationAmenity", new[] { "AccomodationId" });
            DropIndex("dbo.Accomodation", new[] { "CurrencyId" });
            DropIndex("dbo.Accomodation", new[] { "ContactId" });
            DropIndex("dbo.Accomodation", new[] { "CompanyId" });
            DropTable("dbo.HotelDestination");
            DropTable("dbo.Destination");
            DropTable("dbo.Currency");
            DropTable("dbo.Contact");
            DropTable("dbo.Address");
            DropTable("dbo.Company");
            DropTable("dbo.Tax");
            DropTable("dbo.RateTax");
            DropTable("dbo.RatePrice");
            DropTable("dbo.Rate");
            DropTable("dbo.AccomodationType");
            DropTable("dbo.AccomodationUnit");
            DropTable("dbo.AccomodationUnitAmenity");
            DropTable("dbo.Amenity");
            DropTable("dbo.AccomodationAmenity");
            DropTable("dbo.Accomodation");
        }
    }
}
