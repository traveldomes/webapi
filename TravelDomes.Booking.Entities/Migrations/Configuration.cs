namespace TravelDomes.Booking.Entities.Migrations
{
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Model;
    using Microsoft.AspNet.Identity;

    internal sealed class Configuration : DbMigrationsConfiguration<TravelDomes.Booking.Entities.BookingContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(TravelDomes.Booking.Entities.BookingContext context)
        {
            ////if (System.Diagnostics.Debugger.IsAttached == false)
                ////System.Diagnostics.Debugger.Launch();

            try
            {
                var roleStore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);

                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);

                // Add missing roles
                var roleAdmin = roleManager.FindByName("Admin");
                if (roleAdmin == null)
                {
                    roleAdmin = new IdentityRole("Admin");
                    roleManager.Create(roleAdmin);
                }

                var roleProvider = roleManager.FindByName("Provider");
                if (roleProvider == null)
                {
                    roleProvider = new IdentityRole("Provider");
                    roleManager.Create(roleProvider);
                }

                var roleUser = roleManager.FindByName("Standard");
                if (roleUser == null)
                {
                    roleUser = new IdentityRole("Standard");
                    roleManager.Create(roleUser);
                }

                // Create test users
                var user = userManager.FindByName("cristureanu@yahoo.com");
                if (user == null)
                {
                    var newUser = new ApplicationUser()
                    {
                        UserName = "cristureanu@yahoo.com",
                        Email = "cristureanu@yahoo.com",
                        PhoneNumber = "5551234567",
                    };
                    //if (System.Diagnostics.Debugger.IsAttached == false)
                    //    System.Diagnostics.Debugger.Launch();

                    var result = userManager.Create(newUser, "TravelDomes01");

                    if (!result.Succeeded)
                    {
                        Console.WriteLine("Failed to add user admin.");
                    }
                    userManager.SetLockoutEnabled(newUser.Id, false);
                    var code = userManager.GenerateEmailConfirmationToken(newUser.Id);
                    userManager.ConfirmEmail(newUser.Id, code);

                    userManager.AddToRole(newUser.Id, "Admin");
                    userManager.AddToRole(newUser.Id, "Provider");
                    userManager.AddToRole(newUser.Id, "Standard");
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("Failed to seed database.", e.Message);
            }

        }
    }
}
