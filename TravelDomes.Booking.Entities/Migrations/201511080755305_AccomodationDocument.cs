namespace TravelDomes.Booking.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AccomodationDocument : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Document",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                        Size = c.Long(nullable: false),
                        Type = c.String(maxLength: 10),
                        Content = c.Binary(),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedOn = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AccomodationUnitDocument",
                c => new
                    {
                        AccomodationUnitId = c.Int(nullable: false),
                        DocumentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AccomodationUnitId, t.DocumentId })
                .ForeignKey("dbo.AccomodationUnit", t => t.AccomodationUnitId, cascadeDelete: true)
                .ForeignKey("dbo.Document", t => t.DocumentId, cascadeDelete: true)
                .Index(t => t.AccomodationUnitId)
                .Index(t => t.DocumentId);
            
            CreateTable(
                "dbo.AccomodationDocument",
                c => new
                    {
                        AccomodationId = c.Int(nullable: false),
                        DocumentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AccomodationId, t.DocumentId })
                .ForeignKey("dbo.Accomodation", t => t.AccomodationId, cascadeDelete: true)
                .ForeignKey("dbo.Document", t => t.DocumentId, cascadeDelete: true)
                .Index(t => t.AccomodationId)
                .Index(t => t.DocumentId);
            
            AddColumn("dbo.Accomodation", "MainImageId", c => c.Int());
            AddColumn("dbo.AccomodationUnit", "MainImageId", c => c.Int());
            CreateIndex("dbo.Accomodation", "MainImageId");
            CreateIndex("dbo.AccomodationUnit", "MainImageId");
            AddForeignKey("dbo.Accomodation", "MainImageId", "dbo.Document", "Id");
            AddForeignKey("dbo.AccomodationUnit", "MainImageId", "dbo.Document", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AccomodationDocument", "DocumentId", "dbo.Document");
            DropForeignKey("dbo.AccomodationDocument", "AccomodationId", "dbo.Accomodation");
            DropForeignKey("dbo.AccomodationUnitDocument", "DocumentId", "dbo.Document");
            DropForeignKey("dbo.AccomodationUnitDocument", "AccomodationUnitId", "dbo.AccomodationUnit");
            DropForeignKey("dbo.AccomodationUnit", "MainImageId", "dbo.Document");
            DropForeignKey("dbo.Accomodation", "MainImageId", "dbo.Document");
            DropIndex("dbo.AccomodationDocument", new[] { "DocumentId" });
            DropIndex("dbo.AccomodationDocument", new[] { "AccomodationId" });
            DropIndex("dbo.AccomodationUnitDocument", new[] { "DocumentId" });
            DropIndex("dbo.AccomodationUnitDocument", new[] { "AccomodationUnitId" });
            DropIndex("dbo.AccomodationUnit", new[] { "MainImageId" });
            DropIndex("dbo.Accomodation", new[] { "MainImageId" });
            DropColumn("dbo.AccomodationUnit", "MainImageId");
            DropColumn("dbo.Accomodation", "MainImageId");
            DropTable("dbo.AccomodationDocument");
            DropTable("dbo.AccomodationUnitDocument");
            DropTable("dbo.Document");
        }
    }
}
