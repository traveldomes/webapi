namespace TravelDomes.Booking.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IdentifiableEntity : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.HotelDestination", "DestinationId", "dbo.Destination");
            DropPrimaryKey("dbo.Destination");
            AddColumn("dbo.Accomodation", "Status", c => c.Int(nullable: false));
            AddColumn("dbo.AccomodationAmenity", "Status", c => c.Int(nullable: false));
            AddColumn("dbo.Amenity", "Status", c => c.Int(nullable: false));
            AddColumn("dbo.AccomodationUnitAmenity", "Status", c => c.Int(nullable: false));
            AddColumn("dbo.AccomodationUnit", "Status", c => c.Int(nullable: false));
            AddColumn("dbo.AccomodationAvailability", "Status", c => c.Int(nullable: false));
            AddColumn("dbo.AccomodationType", "Status", c => c.Int(nullable: false));
            AddColumn("dbo.Rate", "Status", c => c.Int(nullable: false));
            AddColumn("dbo.RatePrice", "Status", c => c.Int(nullable: false));
            AddColumn("dbo.RateTax", "Status", c => c.Int(nullable: false));
            AddColumn("dbo.Tax", "Status", c => c.Int(nullable: false));
            AddColumn("dbo.Company", "Status", c => c.Int(nullable: false));
            AddColumn("dbo.Address", "Status", c => c.Int(nullable: false));
            AddColumn("dbo.Contact", "Status", c => c.Int(nullable: false));
            AddColumn("dbo.Currency", "Status", c => c.Int(nullable: false));
            AddColumn("dbo.Destination", "Status", c => c.Int(nullable: false));
            AlterColumn("dbo.Destination", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Destination", "Id");
            AddForeignKey("dbo.HotelDestination", "DestinationId", "dbo.Destination", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.HotelDestination", "DestinationId", "dbo.Destination");
            DropPrimaryKey("dbo.Destination");
            AlterColumn("dbo.Destination", "Id", c => c.Int(nullable: false));
            DropColumn("dbo.Destination", "Status");
            DropColumn("dbo.Currency", "Status");
            DropColumn("dbo.Contact", "Status");
            DropColumn("dbo.Address", "Status");
            DropColumn("dbo.Company", "Status");
            DropColumn("dbo.Tax", "Status");
            DropColumn("dbo.RateTax", "Status");
            DropColumn("dbo.RatePrice", "Status");
            DropColumn("dbo.Rate", "Status");
            DropColumn("dbo.AccomodationType", "Status");
            DropColumn("dbo.AccomodationAvailability", "Status");
            DropColumn("dbo.AccomodationUnit", "Status");
            DropColumn("dbo.AccomodationUnitAmenity", "Status");
            DropColumn("dbo.Amenity", "Status");
            DropColumn("dbo.AccomodationAmenity", "Status");
            DropColumn("dbo.Accomodation", "Status");
            AddPrimaryKey("dbo.Destination", "Id");
            AddForeignKey("dbo.HotelDestination", "DestinationId", "dbo.Destination", "Id", cascadeDelete: true);
        }
    }
}
