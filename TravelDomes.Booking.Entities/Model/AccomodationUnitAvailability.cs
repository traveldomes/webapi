﻿namespace TravelDomes.Booking.Entities.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AccomodationAvailability")]
    public partial class AccomodationUnitAvailability : IdentifiableEntity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AccomodationUnitAvailability()
        {
        }

        public int AccomodationUnitId
        {
            get;
            set;
        }


        public DateTime AvailableDate { get; set; }

        public virtual AccomodationUnit AccomodationUnit { get; set; }

        public string UserId { get; set; }

        public virtual ApplicationUser User { get; set; }

        public DateTime LastUpdate { get;  set;}

    }
}
