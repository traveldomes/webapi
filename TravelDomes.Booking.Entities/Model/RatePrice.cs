namespace TravelDomes.Booking.Entities.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RatePrice")]
    public partial class RatePrice : IdentifiableEntity
    {
        public int RateId { get; set; }

        public decimal Value { get; set; }

        public DateTime ValidFrom { get; set; }

        public DateTime ValidTo { get; set; }

        public bool IsValidWeekDay1 { get; set; }

        public bool IsValidWeekDay2 { get; set; }

        public bool IsValidWeekDay3 { get; set; }

        public bool IsValidWeekDay4 { get; set; }

        public bool IsValidWeekDay5 { get; set; }

        public bool IsValidWeekDay6 { get; set; }

        public bool IsValidWeekDay7 { get; set; }

        public virtual Rate Rate { get; set; }
    }
}
