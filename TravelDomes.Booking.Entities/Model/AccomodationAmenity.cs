namespace TravelDomes.Booking.Entities.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AccomodationAmenity")]
    public partial class AccomodationAmenity : IdentifiableEntity
    {
        public int AccomodationId { get; set; }

        public int AmenityId { get; set; }

        public bool IsAvailableToAll { get; set; }

        public bool IsFreeOfCharge { get; set; }

        public virtual Accomodation Accomodation { get; set; }

        public virtual Amenity Amenity { get; set; }
    }
}
