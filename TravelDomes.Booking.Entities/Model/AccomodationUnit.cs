namespace TravelDomes.Booking.Entities.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AccomodationUnit")]
    public partial class AccomodationUnit : IdentifiableEntity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AccomodationUnit()
        {
            AccomodationUnitAmenity = new HashSet<AccomodationUnitAmenity>();
            AccomodationAvailabilityList = new HashSet<AccomodationUnitAvailability>();
            Rate = new HashSet<Rate>();
            Images = new HashSet<Document>();
        }

        public int AccomodationId { get; set; }

        public int Type { get; set; }

        [Required]
        [StringLength(250)]
        public string Name { get; set; }

        public string Description { get; set; }

        public int? MainImageId { get; set; }

        public virtual Accomodation Accomodation { get; set; }

        public virtual AccomodationType AccomodationType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AccomodationUnitAmenity> AccomodationUnitAmenity { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AccomodationUnitAvailability> AccomodationAvailabilityList { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rate> Rate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Document> Images{ get; set; }

        public virtual Document MainImage { get; set; }
    }
}
