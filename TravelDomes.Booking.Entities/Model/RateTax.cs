namespace TravelDomes.Booking.Entities.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RateTax")]
    public partial class RateTax : IdentifiableEntity
    {
        public int RateId { get; set; }

        public int TaxId { get; set; }

        public bool IsIncluded { get; set; }

        public virtual Rate Rate { get; set; }

        public virtual Tax Tax { get; set; }
    }
}
