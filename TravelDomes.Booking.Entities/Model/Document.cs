﻿namespace TravelDomes.Booking.Entities.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Class which holds the information and content of an document.
    /// </summary>
    [Table("Document")]
    public class Document : IdentifiableEntity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Document()
        {
            Accomodations = new HashSet<Accomodation>();
            AccomodationUnits = new HashSet<AccomodationUnit>();
            MainImageAccomodations = new HashSet<Accomodation>();
            MainImageAccomodationUnits = new HashSet<AccomodationUnit>();
        }

        /// <summary>
        /// Gets or sets the name of the document.
        /// </summary>
        [StringLength(250)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the size in bytes.
        /// </summary>
        public long Size { get; set; }

        /// <summary>
        /// Gets or sets the type of the document (xml, jpg, etc).
        /// </summary>
        [StringLength(10)]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the content of the document as an array of bytes.
        /// </summary>
        public byte[] Content { get; set; }

        /// <summary>
        /// Gets or sets the date and time when the document was created.
        /// </summary>
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// Gets or sets the date nad time of the last modiffication of the file.
        /// </summary>
        public DateTime ModifiedOn { get; set; }

        /// <summary>
        /// Gets or sets linked accomodation.
        /// </summary>
        public virtual HashSet<Accomodation> Accomodations { get; private set; }

        /// <summary>
        /// Gets or sets linked accomodation units.
        /// </summary>
        public virtual HashSet<AccomodationUnit> AccomodationUnits { get; set; }

        public virtual HashSet<Accomodation> MainImageAccomodations{ get; set; }

        public virtual HashSet<AccomodationUnit> MainImageAccomodationUnits { get; set; }
    }
}
