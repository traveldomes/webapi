﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelDomes.Booking.Entities.Model
{
    public class IdentifiableEntity : IIdentifiable, IStatus
    {
        public int Id { get; set; }

        public EntityStatus Status { get; set; }
    }
}
