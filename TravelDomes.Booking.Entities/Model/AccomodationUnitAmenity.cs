namespace TravelDomes.Booking.Entities.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AccomodationUnitAmenity")]
    public partial class AccomodationUnitAmenity : IdentifiableEntity
    {
        public int AccomodationUnitId { get; set; }

        public int AmenityId { get; set; }

        public bool IsFreeOfCharge { get; set; }

        public virtual AccomodationUnit AccomodationUnit { get; set; }

        public virtual Amenity Amenity { get; set; }
    }
}
