namespace TravelDomes.Booking.Entities.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Accomodation")]
    public partial class Accomodation: IdentifiableEntity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Accomodation()
        {
            AccomodationAmenity = new HashSet<AccomodationAmenity>();
            AccomodationUnit = new HashSet<AccomodationUnit>();
            Destination = new HashSet<Destination>();
            Images = new HashSet<Document>();
        }

        [StringLength(250)]
        public string Name { get; set; }

        public int CompanyId { get; set; }

        [Required]
        public string WebSiteUrl { get; set; }

        public int ContactId { get; set; }

        public int AddressId { get; set; }

        public int ReservationDeliveryType { get; set; }

        [Required]
        [StringLength(254)]
        public string ReservationEmail { get; set; }

        [Required]
        [StringLength(15)]
        public string ReservationFax { get; set; }

        public decimal? LocalizationLong { get; set; }

        public decimal? LocalizationLat { get; set; }

        public int CurrencyId { get; set; }

        public int? MainImageId { get; set; }

        public virtual Company Company { get; set; }

        public virtual Contact Contact { get; set; }

        public virtual Currency Currency { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AccomodationAmenity> AccomodationAmenity { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AccomodationUnit> AccomodationUnit { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Destination> Destination { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Document> Images { get; set; }

        public virtual Document MainImage { get; set; }
    }
}
