﻿namespace TravelDomes.Booking.Entities
{
    public enum EntityStatus
    {
        
        Active = 0,
        Deleted = 1,
        Canceled = 2,
        Suspended = 3
    }
}