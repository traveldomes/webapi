﻿namespace TravelDomes.Booking.Entities
{
    public interface IStatus
    {
        EntityStatus Status { get; set; }
    }
}
