namespace TravelDomes.Booking.Entities
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Model;
    using Microsoft.AspNet.Identity.EntityFramework;

    public partial class BookingContext : IdentityDbContext<ApplicationUser>
    {
        public BookingContext()
            : base("BookingContext", throwIfV1Schema: false)
        {
            this.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
        }

        public static BookingContext Create()
        {
            return new BookingContext();
        }

        public virtual DbSet<Accomodation> Accomodation { get; set; }
        public virtual DbSet<AccomodationAmenity> AccomodationAmenity { get; set; }
        public virtual DbSet<AccomodationType> AccomodationType { get; set; }
        public virtual DbSet<AccomodationUnit> AccomodationUnit { get; set; }
        public virtual DbSet<AccomodationUnitAvailability> AccomodationUnitAvailability { get; set; }
        public virtual DbSet<AccomodationUnitAmenity> AccomodationUnitAmenity { get; set; }
        public virtual DbSet<Address> Address { get; set; }
        public virtual DbSet<Amenity> Amenity { get; set; }
        public virtual DbSet<Company> Company { get; set; }
        public virtual DbSet<Contact> Contact { get; set; }
        public virtual DbSet<Currency> Currency { get; set; }
        public virtual DbSet<Destination> Destination { get; set; }
        public virtual DbSet<Rate> Rate { get; set; }
        public virtual DbSet<RatePrice> RatePrice { get; set; }
        public virtual DbSet<RateTax> RateTax { get; set; }
        public virtual DbSet<Tax> Tax { get; set; }
        public virtual DbSet<Document> Documents { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Accomodation>()
                .Property(e => e.LocalizationLong)
                .HasPrecision(11, 8);

            modelBuilder.Entity<Accomodation>()
                .Property(e => e.LocalizationLat)
                .HasPrecision(11, 8);

            modelBuilder.Entity<Accomodation>()
                .HasMany(e => e.AccomodationAmenity)
                .WithRequired(e => e.Accomodation)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Accomodation>()
                .HasMany(e => e.AccomodationUnit)
                .WithRequired(e => e.Accomodation)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Accomodation>()
                .HasMany(e => e.Images)
                .WithMany(e => e.Accomodations)
                .Map(m => m.ToTable("AccomodationDocument").MapLeftKey("AccomodationId").MapRightKey("DocumentId"));

            modelBuilder.Entity<Accomodation>()
                .HasMany(e => e.Destination)
                .WithMany(e => e.Accomodation)
                .Map(m => m.ToTable("HotelDestination").MapLeftKey("HotelId").MapRightKey("DestinationId"));

            modelBuilder.Entity<AccomodationType>()
                .HasMany(e => e.AccomodationUnit)
                .WithRequired(e => e.AccomodationType)
                .HasForeignKey(e => e.Type)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AccomodationUnit>()
                .HasMany(e => e.AccomodationUnitAmenity)
                .WithRequired(e => e.AccomodationUnit)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AccomodationUnit>()
                .HasMany(e => e.AccomodationAvailabilityList)
                .WithRequired(e => e.AccomodationUnit)
                .HasForeignKey(e => e.AccomodationUnitId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AccomodationUnit>()
                .HasMany(e => e.Rate)
                .WithRequired(e => e.AccomodationUnit)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AccomodationUnit>()
                .HasMany(e => e.Images)
                .WithMany(e => e.AccomodationUnits)
                .Map(m => m.ToTable("AccomodationUnitDocument").MapLeftKey("AccomodationUnitId").MapRightKey("DocumentId"));

            modelBuilder.Entity<Document>()
                .HasMany(e => e.MainImageAccomodations)
                .WithOptional(e => e.MainImage)
                .HasForeignKey(e => e.MainImageId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Document>()
                .HasMany(e => e.MainImageAccomodationUnits)
                .WithOptional(e => e.MainImage)
                .HasForeignKey(e => e.MainImageId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Address>()
                .HasMany(e => e.Company)
                .WithRequired(e => e.Address)
                .HasForeignKey(e => e.BillingAddressId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Amenity>()
                .HasMany(e => e.AccomodationAmenity)
                .WithRequired(e => e.Amenity)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Amenity>()
                .HasMany(e => e.AccomodationUnitAmenity)
                .WithRequired(e => e.Amenity)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.Accomodation)
                .WithRequired(e => e.Company)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.PrimaryPhone)
                .IsFixedLength();

            modelBuilder.Entity<Contact>()
                .Property(e => e.MobilePhone)
                .IsFixedLength();

            modelBuilder.Entity<Contact>()
                .Property(e => e.Fax)
                .IsFixedLength();

            modelBuilder.Entity<Contact>()
                .HasMany(e => e.Accomodation)
                .WithRequired(e => e.Contact)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Contact>()
                .HasMany(e => e.Company)
                .WithRequired(e => e.Contact)
                .HasForeignKey(e => e.BillingContactId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Currency>()
                .HasMany(e => e.Accomodation)
                .WithRequired(e => e.Currency)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Destination>()
                .Property(e => e.Latitude)
                .HasPrecision(11, 8);

            modelBuilder.Entity<Destination>()
                .Property(e => e.Longitude)
                .HasPrecision(11, 8);

            modelBuilder.Entity<Rate>()
                .HasMany(e => e.RatePrice)
                .WithRequired(e => e.Rate)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Rate>()
                .HasMany(e => e.RateTax)
                .WithRequired(e => e.Rate)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<RatePrice>()
                .Property(e => e.Value)
                .HasPrecision(18, 4);

            modelBuilder.Entity<Tax>()
                .Property(e => e.Value)
                .HasPrecision(18, 4);

            modelBuilder.Entity<Tax>()
                .HasMany(e => e.RateTax)
                .WithRequired(e => e.Tax)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationUser>()
               .HasMany(e => e.AccomodationUnitAvailability)
               .WithRequired(e => e.User)
               .HasForeignKey(e => e.UserId)
               .WillCascadeOnDelete(false);
        }
    }
}
