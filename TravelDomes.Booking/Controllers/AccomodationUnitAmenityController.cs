﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TravelDomes.Booking.Entities;
using TravelDomes.Booking.Entities.Model;

namespace TravelDomes.Booking.Api.Controllers
{
    public class AccomodationUnitAmenityController : BaseController<AccomodationUnitAmenity>
    {
        public AccomodationUnitAmenityController(BookingContext context) : base(context)
        {
        }
    }
}
