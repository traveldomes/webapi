﻿namespace TravelDomes.Booking.Api.Controllers
{
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using System.Net;
    using System.Web.Http;
    using System.Web.Http.Description;
    using TravelDomes.Booking.Entities;

    public class BaseController<T> : ApiController where T : class, IIdentifiable
    {
        private BookingContext db = new BookingContext();

        public BaseController()
        {
            this.db.Configuration.LazyLoadingEnabled = false;
        }

        public BaseController(BookingContext context)
        {
            this.db = context;
        }

        protected BookingContext DataContext
        {
            get
            {
                return this.db;
            }
        }

        // GET: api/Accomodations
        public IQueryable<T> Get()
        {
            return db.Set<T>();
        }

        // GET: api/Accomodations/5
        public virtual IHttpActionResult Get(int id)
        {
            T item = db.Set<T>().Find(id);
            if (item == null)
            {
                return NotFound();
            }

            return Ok(item);
        }

        // PUT: api/Accomodations/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Put(int id, T item)
        { 
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != item.Id)
            {
                return BadRequest();
            }

            db.Entry(item).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Accomodations
        public IHttpActionResult Post(T item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Set<T>().Add(item);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = item.Id }, item);
        }

        // DELETE: api/Accomodations/5
        public IHttpActionResult Delete(int id)
        {
            T item = db.Set<T>().Find(id);
            if (item == null)
            {
                return NotFound();
            }

            IStatus entityStatus = item as IStatus;
            if (entityStatus != null)
            {
                entityStatus.Status = EntityStatus.Deleted;
                db.Entry(item).State = EntityState.Modified;
            }
            else
            {
                db.Set<T>().Remove(item);
            }

            db.SaveChanges();

            return Ok(item);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ItemExists(int id)
        {
            return db.Set<T>().Count(e => e.Id == id) > 0;
        }
    }

}
