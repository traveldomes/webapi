﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TravelDomes.Booking.Entities;
using TravelDomes.Booking.Entities.Model;

namespace TravelDomes.Booking.Api.Controllers
{
    [Authorize(Roles = "Provider")]
    public class AmenityController : BaseController<Amenity>
    {
        public AmenityController(BookingContext context) : base(context)
        {
        }
    }
}
