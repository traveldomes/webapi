﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TravelDomes.Booking.Api.Controllers;
using TravelDomes.Booking.Api.Models.Destination;
using TravelDomes.Booking.Entities;
using TravelDomes.Booking.Entities.Model;

namespace TravelDomes.Booking.Controllers
{
    public class DestinationController : BaseController<Destination>
    {
        public DestinationController()
        {

        }

        public DestinationController(BookingContext context) : base(context)
        {
        }

        /// <summary>
        /// Returns a list of key value pairs representing the destinations starting with the letters from *partialName* parameter.
        /// </summary>
        /// <param name="partialName">Part of the destination used as a searching term</param>
        /// <returns>A list of key-value pairs representing the id and full name of the destinations wgich start the searching term.</returns>
        public IEnumerable<DestinationResultItem> GetByLikeName(string partialName)
        {
            int searchTermSize = 3;

            Int32.TryParse(ConfigurationManager.AppSettings["minimumLengthSearchTerm"], out searchTermSize);

            if (partialName.Length < searchTermSize)
            {
                return new List<DestinationResultItem>();
            }

            var destinations =
                this.DataContext.Destination
                .Where(s => s.Name.StartsWith(partialName))
                .Select(s => new DestinationResultItem{ Id = s.Id, Name = s.Name })
                .ToList();
            return destinations;
        }

        

    }
}
