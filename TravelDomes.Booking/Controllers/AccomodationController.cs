﻿namespace TravelDomes.Booking.Api.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using TravelDomes.Booking.Api.Models;
    using Models.AccomodationUnit;
    using TravelDomes.Booking.Entities;
    using TravelDomes.Booking.Entities.Model;
    using Models.Accomodation;


    /// <summary>
    /// Defines the controller responsible for handling Accomodation entiries.
    /// </summary>
    //[Authorize(Roles = "Provider")]
    public class AccomodationController : BaseController<Accomodation>
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public AccomodationController()
        {

        }

        /// <summary>
        /// Creates a new instance of the AccomodationController.
        /// </summary>
        /// <param name="context">The data context used by the controller.</param>
        public AccomodationController(BookingContext context) : base(context)
        {
        }

        /// <summary>
        /// Searches for the all available accomodation units between 2 dates for a specific destination.
        /// </summary>
        /// <param name="destinationId">Id of the destination for which the search is made.</param>
        /// <param name="startDate">The start date for the availability.</param>
        /// <param name="endDate">THe end date of the availability.</param>
        /// <returns></returns>
        public IEnumerable<AccomodationUnitSearchResult> GetUnitsByPeriod(int destinationId, DateTime startDate, DateTime endDate)
        {
            var availability = this.DataContext.AccomodationUnitAvailability.Include("AccomodationUnit.Accomodation.Destination")
                .Where(s => 
                    s.AvailableDate < endDate 
                    && s.AvailableDate >= startDate 
                    && s.Status == EntityStatus.Active
                    && s.AccomodationUnit.Accomodation.Destination.Any(e => e.Id == destinationId))
                .Select(s => new AccomodationUnitSearchResult()
                {
                    AccomodationId = s.AccomodationUnit.AccomodationId,
                    AccomodationName = s.AccomodationUnit.Accomodation.Name,
                    AccomodationUnitId = s.AccomodationUnitId,
                    AccomodationUnitName = s.AccomodationUnit.Name,
                    AvailableDate = s.AvailableDate
                })
                .ToList();

            return availability;

        }

        /// <summary>
        /// Gets all accomodations for a destination.
        /// </summary>
        /// <param name="destinationId">The id of the destination.</param>
        /// <returns></returns>
        public IEnumerable<AccomodationSearchResult> GetAllByDestination(int destinationId)
        {
            var destinationItem = this.DataContext.Destination.Include("Accomodation").FirstOrDefault(s => s.Id == destinationId && s.Status == EntityStatus.Active);

            if (destinationItem == null)
            {
                return new List<AccomodationSearchResult>();
            }

            return destinationItem.Accomodation
                .Where(s => s.Status == EntityStatus.Active)
                .Select(s => new AccomodationSearchResult()
                {
                    AccomodationId = s.Id,
                    AccomodationName = s.Name
                })
                .ToList();

        }

    }
}