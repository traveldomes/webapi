﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TravelDomes.Booking.Entities;
using TravelDomes.Booking.Entities.Model;

namespace TravelDomes.Booking.Api.Controllers
{
    public class RateTaxController : BaseController<RateTax>
    {
        public RateTaxController(BookingContext context) : base(context)
        {
        }
    }
}
