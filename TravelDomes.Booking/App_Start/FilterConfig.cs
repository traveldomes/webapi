﻿using System.Web;
using System.Web.Mvc;

namespace TravelDomes.Booking
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
