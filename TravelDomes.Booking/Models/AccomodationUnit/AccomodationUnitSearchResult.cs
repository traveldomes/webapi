﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelDomes.Booking.Api.Models.AccomodationUnit
{
    public class AccomodationUnitSearchResult
    {
        public int AccomodationId { get; set; }
        public string AccomodationName { get; set; }
        public int AccomodationUnitId { get; set; }
        public string AccomodationUnitName { get; set; }
        public DateTime? AvailableDate { get; internal set; }
    }
}