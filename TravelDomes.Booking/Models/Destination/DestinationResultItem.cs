﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelDomes.Booking.Api.Models.Destination
{
    public class DestinationResultItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}