﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelDomes.Booking.Api.Models.Accomodation
{
    public class AccomodationSearchResult
    {
        public int AccomodationId { get; set; }
        public string AccomodationName { get; set; }
    }
}