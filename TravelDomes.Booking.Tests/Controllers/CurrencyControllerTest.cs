﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using TravelDomes.Booking.Api.Controllers;
using TravelDomes.Booking.Entities;
using TravelDomes.Booking.Entities.Model;

namespace TravelDomes.Api.Tests.Controllers
{
    [TestClass]
    public class CurrencyControllerTest: BaseControllerTest<Currency>
    {
        [TestMethod]
        public void GetAll()
        {
            var data = new List<Currency>
            {
                new Currency { Id = 1, Name = "BBB", IsDefault = true },
                new Currency { Id = 2, Name = "ZZZ", IsDefault = false },
                new Currency { Id = 3, Name = "AAA" },
            }.AsQueryable();

            var mockContext = PrepareQueryableMockSet(data, c => c.Currency);

            var service = new CurrencyController(mockContext.Object);
            var currencies = service.Get().ToList();

            Assert.AreEqual(3, currencies.Count());
            Assert.AreEqual("BBB", currencies[0].Name);
            Assert.AreEqual(1, currencies[0].Id);
        }

        [TestMethod]
        public void Post()
        {
            Mock<DbSet<Currency>> mockSet = new Mock<DbSet<Currency>>();
            var mockContext = BaseControllerTest<Currency>.CreateUpdateableMockSet(mockSet, new List<Currency>().AsQueryable());

            var service = new CurrencyController(mockContext.Object);
            service.Post(new Currency() { Id = 1, Name = "Dollar", IsDefault = true });

            mockSet.Verify(m => m.Add(It.IsAny<Currency>()), Times.Once());
            mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }

        protected override BaseController<Currency> CreateController(BookingContext context)
        {
            return new CurrencyController(context);
        }

        //[TestMethod]
        //public virtual void GetReturnsNotFound()
        //{
        //    //Mock<DbSet<T>> mockSet = new Mock<DbSet<T>>();
        //    // Arrange
        //    var mockRepository = PrepareQueryableMockSet(new List<Currency>().AsQueryable());
        //    var controller = new CurrencyController(mockRepository.Object);

        //    // Act
        //    IHttpActionResult actionResult = controller.Get(-1);

        //    // Assert
        //    Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        //}

    }
}
