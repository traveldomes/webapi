﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using TravelDomes.Booking.Api.Controllers;
using TravelDomes.Booking.Entities;

namespace TravelDomes.Api.Tests.Controllers
{
    public abstract class BaseControllerTest<T> where T : class, IIdentifiable
    {
        protected static Mock<BookingContext> PrepareQueryableMockSet<R>(IQueryable<R> data, System.Linq.Expressions.Expression<Func<BookingContext, DbSet<R>>> tableMap = null) where R : class, IIdentifiable
        {
            var mockSet = new Mock<DbSet<R>>();

            mockSet.As<IQueryable<R>>().Setup(m => m.Provider).Returns(data.Provider);

            mockSet.As<IQueryable<R>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<R>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<R>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());
            mockSet.Setup(m => m.Include(It.IsAny<String>())).Returns(mockSet.Object);

            var mockContext = new Mock<BookingContext>();

            mockContext.Setup(c => c.Set<R>()).Returns(mockSet.Object);

            if (tableMap != null)
            {
                mockContext.Setup(tableMap).Returns(mockSet.Object);
            }



            return mockContext;
        }

        protected static Mock<BookingContext> CreateUpdateableMockSet(Mock<DbSet<T>> mockSet, IQueryable<T> data)
        {
            mockSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(data.Provider);

            mockSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<BookingContext>();
            mockContext.Setup(m => m.Set<T>()).Returns(mockSet.Object);

            return mockContext;
        }

        protected abstract BaseController<T> CreateController(BookingContext context);

        [TestMethod]
        public virtual void GetReturnsNotFound()
        {
            //Mock<DbSet<T>> mockSet = new Mock<DbSet<T>>();
            // Arrange
            var mockRepository = PrepareQueryableMockSet<T>(new List<T>().AsQueryable());
            var controller = CreateController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Get(-1);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }
    }
}
