﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TravelDomes.Booking.Entities.Model;
using TravelDomes.Booking.Entities;
using TravelDomes.Booking.Api.Controllers;
using System.Collections.Generic;
using System.Linq;

namespace TravelDomes.Api.Tests.Controllers
{
    [TestClass]
    public class AccomodationControllerTest : BaseControllerTest<Accomodation>
    {
        protected override BaseController<Accomodation> CreateController(BookingContext context)
        {
            return new AccomodationController(context);
        }

        
    }
}
