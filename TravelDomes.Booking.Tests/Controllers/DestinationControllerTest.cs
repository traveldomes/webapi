﻿namespace TravelDomes.Api.Tests.Controllers
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System.Collections.Generic;
    using System.Linq;
    using TravelDomes.Booking.Api.Controllers;
    using TravelDomes.Booking.Controllers;
    using TravelDomes.Booking.Entities;
    using TravelDomes.Booking.Entities.Model;

    [TestClass]
    public class DestinationControllerTest : BaseControllerTest<Destination>
    {
        protected override BaseController<Destination> CreateController(BookingContext context)
        {
            return new DestinationController(context);
        }

        [TestMethod]
        public void  SearchDestinationsByName()
        {
            var hotel1 = new Accomodation { Id = 2, Name = "ZZZ", Status = EntityStatus.Deleted };
            var hotel2 = new Accomodation { Id = 3, Name = "AAA", Status = EntityStatus.Active };
            var hotel3 = new Accomodation { Id = 1, Name = "BBB", Status = EntityStatus.Active };

            

            Destination destination1 = new Destination()
            {
                Id = 1,
                Latitude = 2,
                Name = "Home",
                Longitude = 3,
                Status = EntityStatus.Active
            };
            destination1.Accomodation.Add(hotel1);
            destination1.Accomodation.Add(hotel2);
            destination1.Accomodation.Add(hotel3);

            Destination destination2 = new Destination()
            {
                Id = 2,
                Latitude = 2,
                Name = "AWAY",
                Longitude = 3,
                Status = EntityStatus.Active
            };

            var data = new List<Destination>
            {
                destination1,
                destination2
            }.AsQueryable();

            var mockContext = PrepareQueryableMockSet<Destination>(data, c => c.Destination);

            var service = new DestinationController(mockContext.Object);

            var hotels = service.GetByLikeName("Home"); // response as OkNegotiatedContentResult<List<DestinationResultItem>> ;

            Assert.AreEqual(hotels.Count(), 1);
        }
    }
}
