﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Opentravel.Messages;

namespace TravelDomes.Booking.Ota
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class TravelDomesBookingService : IOTAService
    {
        public OTA_HotelAvailRS GetHotelAvailable(OTA_HotelAvailRQ request)
        {

            OTA_HotelAvailRSRoomStays[] items = new OTA_HotelAvailRSRoomStays[]
                {
                    new OTA_HotelAvailRSRoomStays()
                    {
                        RoomStay = new OTA_HotelAvailRSRoomStaysRoomStay[]
                        {
                            new OTA_HotelAvailRSRoomStaysRoomStay()
                            {
                                AvailabilityStatus = RateIndicatorType.OnRequest,
                                AvailabilityStatusSpecified = true,
                                BasicPropertyInfo = new RoomStayTypeBasicPropertyInfo(),
                                GuestCounts = new GuestCountType()
                                {
                                    GuestCount = new GuestCountTypeGuestCount[]
                                    {
                                        new GuestCountTypeGuestCount { Age = "12", Count = "1"}
                                    }        
                                },
                                RoomTypes = new RoomTypeType[]
                                {
                                    new RoomTypeType()
                                    {
                                        Amenities = new RoomAmenityPrefType[]
                                        {
                                            new RoomAmenityPrefType
                                            {
                                                PreferLevel = PreferLevelType.Preferred, 
                                                Quantity = "1",
                                                RoomAmenity = "Flat TV",
                                                RoomGender = RoomAmenityPrefTypeRoomGender.MaleAndFemale,
                                                RoomGenderSpecified = true,

                                            }
                                        }
                                    }
                                }
                            }
                        },

                        MoreIndicator = ""
                    },
                    new OTA_HotelAvailRSRoomStays()
                    {
                        RoomStay = new OTA_HotelAvailRSRoomStaysRoomStay[]
                        {
                            new OTA_HotelAvailRSRoomStaysRoomStay()
                            {
                                AvailabilityStatus = RateIndicatorType.OnRequest,
                                AvailabilityStatusSpecified = true,
                                BasicPropertyInfo = new RoomStayTypeBasicPropertyInfo()
                            }
                        },
                    }
                };
        
            return new OTA_HotelAvailRS()
            {
                Target = MessageAcknowledgementTypeTarget.Test,
                SearchCacheLevel = OTA_HotelAvailRSSearchCacheLevel.Live,
                POS = new SourceType[] { new SourceType() { } },
                Items = items,
                SearchCacheLevelSpecified = true,
                TransactionStatusCode = MessageAcknowledgementTypeTransactionStatusCode.End,
                TransactionStatusCodeSpecified = true,


            };
        }

        //public string GetData(int value)
        //{
        //    return string.Format("You entered: {0}", value);
        //}

        //public CompositeType GetDataUsingDataContract(CompositeType composite)
        //{
        //    if (composite == null)
        //    {
        //        throw new ArgumentNullException("composite");
        //    }
        //    if (composite.BoolValue)
        //    {
        //        composite.StringValue += "Suffix";
        //    }
        //    return composite;
        //}

        //void IOTAService.GetAvailable(int value)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
