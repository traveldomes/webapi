USE [TravelDomes]
GO


-- CLEAN UP

DELETE FROM AccomodationAvailability
DELETE FROM AccomodationUnit
DELETE FROM Destination
DELETE FROM Accomodation
DELETE FROM HotelDestination
DELETE FROM Company

DELETE FROM Contact
DELETE FROM [Address]
DELETE FROM Currency
DELETE FROM AccomodationType
DELETE FROM dbo.AccomodationDocument
DELETE FROM dbo.AccomodationUnitDocument
DELETE FROM Document

GO

DBCC CHECKIDENT ('dbo.Destination', RESEED, 0) 
GO

DBCC CHECKIDENT ('dbo.Address', RESEED, 0) 
GO
DBCC CHECKIDENT ('dbo.Contact', RESEED, 0) 
GO
DBCC CHECKIDENT ('dbo.Company', RESEED, 0) 
GO
DBCC CHECKIDENT ('dbo.Currency', RESEED, 0) 
GO
DBCC CHECKIDENT ('dbo.Accomodation', RESEED, 0) 
GO
DBCC CHECKIDENT ('dbo.AccomodationUnit', RESEED, 0)
GO
DBCC CHECKIDENT ('dbo.[AccomodationAvailability]', RESEED, 0)
GO
DBCC CHECKIDENT ('dbo.[AccomodationType]', RESEED, 0)
GO
DBCC CHECKIDENT ('dbo.[Document]', RESEED, 0)
GO


DECLARE @addressID AS INT
DECLARE @companyId AS int
DECLARE @contactId AS int
DECLARE @currencyId AS int
DECLARE @accomodatioId AS int 
DECLARE @destinationId AS INT
DECLARE @doubleRoomId AS INT
DECLARE @singleRoomId AS INT
DECLARE @accomodationTypeId AS Int
DECLARE @documentId AS int




INSERT INTO [dbo].[Destination]
           (
           [Name]
           ,[Latitude]
           ,[Longitude]
           ,[IsTop]
           ,[Status])
     VALUES
           (
           'Home'
           ,12.222
           ,333.33
           ,0
           ,0)

SET @destinationId = @@IDENTITY

INSERT INTO [dbo].[Address]
           ([CountryId]
           ,[CityName]
           ,[StreetLine1]
           ,[StreetLine2]
           ,[PostalCode]
           ,[Status])
     VALUES
           (1
           , 'City Name'
           ,'<StreetLine1, nvarchar(250),>'
           ,'<StreetLine2, nvarchar(250),>'
           ,'<PostalCode, (20),>'
           ,0)

SET @addressId = @@IDENTITY

INSERT INTO [dbo].[Contact]
           ([FirstName]
           ,[MiddleName]
           ,[LastName]
           ,[Salutation]
           ,[PrimaryPhone]
           ,[MobilePhone]
           ,[Fax]
           ,[Email]
           ,[Status])
     VALUES
           ('<FirstName, nvarchar(250),>'
           ,'<MiddleName, nvarchar(250),>'
           ,'<LastName, nvarchar(250),>'
           ,'<Salutation, nvarchar(50),>'
           ,'<Primary(15),>'
           ,'<MobileP(15),>'
           ,'<Fax(15),>'
           ,'<Email, nvarchar(254),>'
           ,0)

SET @contactId = @@IDENTITY

INSERT INTO [dbo].[Company]
           ([Name]
           ,[IdentificationCode]
           ,[BillingAddressId]
           ,[BillingContactId]
           ,[Status])
     VALUES
           ('<Name, nvarchar(250),>'
           ,'<IdentificationCodchar(50),>'
           ,@addressID
           ,@contactId
           ,0)

SET @companyId = @@IDENTITY

INSERT INTO [dbo].[Currency]
           ([Name]
           ,[IsDefault]
           ,[Status])
     VALUES
           ('<Name, nvarchar(250),>'
           ,0
           ,1)

SET @currencyId = @@IDENTITY

INSERT INTO [dbo].[Document]
           ([Name]
           ,[Size]
           ,[Type]
           ,[Content]
           ,[CreatedOn]
           ,[ModifiedOn]
           ,[Status])
     VALUES
           ('Document 1'
           ,1024
           ,'jpeg'
           ,''
           ,'2015-01-01'
           ,'2015-01-02'
           ,1)


SET @documentId = @@IDENTITY

INSERT INTO [dbo].[Accomodation]
           ([Name]
           ,[CompanyId]
           ,[WebSiteUrl]
           ,[ContactId]
           ,[AddressId]
           ,[ReservationDeliveryType]
           ,[ReservationEmail]
           ,[ReservationFax]
           ,[LocalizationLong]
           ,[LocalizationLat]
           ,[CurrencyId]
           ,[Status]
		   ,[MainImageId])
     VALUES
           ('COMFORT HOTELL RUNAWAY'
           ,@companyId
           ,'http://'
           ,@contactId
           ,@addressID
           ,2
           ,'email'
           ,'123456'
           ,22.33
           ,33.44
           ,@currencyId
           ,0
		   ,@documentId)

SET @accomodatioId = @@IDENTITY

INSERT INTO [dbo].[HotelDestination]
           ([HotelId]
           ,[DestinationId])
     VALUES
           (@accomodatioId
           ,@destinationId)

INSERT INTO [dbo].[AccomodationType]
           ([Name]
           ,[Description]
           ,[Status])
     VALUES
           ('HOTEL'
           ,'A HOTEL'
           ,0)

SET @accomodationTypeId = @@IDENTITY

INSERT INTO [dbo].[AccomodationUnit]
           ([AccomodationId]
           ,[Type]
           ,[Name]
           ,[Description]
           ,[Status]
		   ,MainImageId)
     VALUES
           (@accomodatioId
           ,@accomodationTypeId
           ,'Double room'
           ,'This is a double room'
           ,0
		   ,@documentId)

SET @doubleRoomId = @@IDENTITY

INSERT INTO [dbo].[AccomodationUnit]
           ([AccomodationId]
           ,[Type]
           ,[Name]
           ,[Description]
           ,[Status])
     VALUES
           (@accomodatioId
           ,@accomodationTypeId
           ,'Single room'
           ,'THis is a single room'
           ,0)

SET @singleRoomId = @@IDENTITY

INSERT INTO [dbo].[AccomodationAvailability]
           ([AccomodationUnitId]
           ,[AvailableDate]
           ,[UserId]
           ,[LastUpdate]
           ,[Status])
     VALUES
           (@singleRoomId
           ,'2015-01-01'
           ,'b6cf061d-fb03-4bc5-b523-c2e13aff7fff'
           ,'2015-12-12'
           ,1)

INSERT INTO [dbo].[AccomodationAvailability]
           ([AccomodationUnitId]
           ,[AvailableDate]
           ,[UserId]
           ,[LastUpdate]
           ,[Status])
     VALUES
           (@singleRoomId
           ,'2015-01-03'
           ,'b6cf061d-fb03-4bc5-b523-c2e13aff7fff'
           ,'2015-12-14'
           ,1)

INSERT INTO [dbo].[AccomodationAvailability]
           ([AccomodationUnitId]
           ,[AvailableDate]
           ,[UserId]
           ,[LastUpdate]
           ,[Status])
     VALUES
           (@singleRoomId
           ,'2015-01-02'
           ,'b6cf061d-fb03-4bc5-b523-c2e13aff7fff'
           ,'2015-12-13'
           ,1)


GO